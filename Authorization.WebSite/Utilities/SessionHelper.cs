﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authorization.WebSite.Utilities
{
    public static class SessionHelper
    {
        public static string UserName {
            get
            {
                return (HttpContext.Current.Session["UserName"] ?? "marevalo").ToString();
            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }
    }
}