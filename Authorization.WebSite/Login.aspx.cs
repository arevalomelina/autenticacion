﻿using Authorization.WebSite.AuthorizationServices;
using Authorization.WebSite.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Authorization.WebSite
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    StatusText.Text = string.Format("Hello {0}!!", User.Identity.GetUserName());
                    LoginStatus.Visible = true;
                    SessionHelper.UserName = User.Identity.GetUserName();
                    Response.Redirect("~/View/ListarUsuarios.aspx");
                }
                else
                {
                    LoginForm.Visible = true;
                }

                if (Request.QueryString.Count > 0)
                {
                    var url = Request.QueryString["ReturnUrl"].ToString();
                    if (!string.IsNullOrWhiteSpace(url))
                    {
                        LoginStatus.Visible = true;
                        StatusText.Text = "La pagina " + url + " a la que intenta acceder necesita que se autentique";
                    }
                }

            }
        }

        protected void SignIn(object sender, EventArgs e)
        {
            var userStore = new MyUserStore<Usuario>();
            var userManager = new UserManager<Usuario>(userStore);
            var user = userManager.Find(UserName.Text, Password.Text);

            if (user != null)
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);
                SessionHelper.UserName = UserName.Text;
                Response.Redirect("~/View/ListarUsuarios.aspx");
            }
            else
            {
                StatusText.Text = "Invalid username or password.";
                LoginStatus.Visible = true;
            }
        }

    }
}