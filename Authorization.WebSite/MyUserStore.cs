﻿using Authorization.WebSite.AuthorizationServices;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Authorization.WebSite
{
    public class MyUserStore<TUser> : IUserStore<TUser>, IDisposable, IUserPasswordStore<TUser>
 where TUser : Usuario
    {
        public Task CreateAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<TUser> FindByIdAsync(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<TUser> FindByNameAsync(string userName)
        {
            using (var svc = new AuthorizationServiceClient())
            {
                TUser user = (TUser)svc.ObtenerUsuario(userName);
                return Task.FromResult<TUser>(user);
            }
        }

        public Task<string> GetPasswordHashAsync(TUser user)
        {

            return Task.FromResult<string>(user.Password);
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(TUser user)
        {
            throw new NotImplementedException();
        }
    }

   
}