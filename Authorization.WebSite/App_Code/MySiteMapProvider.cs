﻿using Authorization.WebSite.AuthorizationServices;
using Authorization.WebSite.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authorization.WebSite.App_Code
{
    public class MySiteMapProvider : StaticSiteMapProvider
    {
        private SiteMapNode parentNode { get; set; }
        List<Menu> menus;
        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                using (var svc = new AuthorizationServiceClient())
                {
                    menus = svc.ObtenerMenu(SessionHelper.UserName).ToList();
                    parentNode = HttpContext.Current.Cache["SiteMap"] as SiteMapNode;
                    if (parentNode == null)
                    {
                        base.Clear();
                        parentNode = new SiteMapNode(this,
                                                "Menu",
                                                null,
                                                "Menu");

                        AddNode(parentNode);
                        List<Menu> menuPadres = menus.Where(p => p.ParentId == 0).ToList();
                        AddChilds(parentNode, menuPadres);
                        HttpContext.Current.Cache.Insert("SiteMap", parentNode);
                    }
                }
                
                return parentNode;
            }
        }

        private void AddChilds(SiteMapNode parentNode, List<Menu> childs)
        {
            List<Menu> menuHijos = new List<Menu>();
            foreach (var item in childs)
            {
                SiteMapNode childNode = new SiteMapNode(this,
                                    item.Nombre,
                                    item.Ruta,
                                    item.Nombre);

                AddNode(childNode, parentNode);
                menuHijos = this.menus.Where(p => p.ParentId == item.AsegurableId).ToList();
                if (menuHijos.Count > 0)
                    AddChilds(childNode, menuHijos);
            }
        }
        protected override SiteMapNode GetRootNodeCore()
        {
            return BuildSiteMap();
        }
    }
}