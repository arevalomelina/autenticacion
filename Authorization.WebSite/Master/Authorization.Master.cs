﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Authorization.WebSite.Master
{
    public partial class Authorization : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            menuMaster.DataSource = smdsAuthorization;

            menuMaster.DataBind();
            litMenu.Text = @"<ul class=""navbar - nav mr - auto"">";
            foreach (MenuItem item in menuMaster.Items)
            {
                CrearMenu(item);
            }

            litMenu.Text += "</ul>";
        }

        protected void SignOut(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/Login.aspx");
        }
        private void CrearMenu(MenuItem men)
        {
            string src = (string.IsNullOrEmpty(men.NavigateUrl) ? "#" : men.NavigateUrl.Replace("~", "."));
            
            if (men.ChildItems != null && men.ChildItems.Count > 0)
            {
                litMenu.Text += @"<li class=""nav-item dropdown""><a class=""nav-link dropdown-toggle"" 
                                    href=""#"" id=""navbarDropdown"" role=""button"" data-toggle=""dropdown"" 
                                    aria-haspopup=""true"" aria-expanded=""false"">" + men.Value + @"</a>" +
                                    @"<div class=""dropdown - menu"" aria-labelledby=""navbarDropdown"">";
                foreach (MenuItem item in men.ChildItems)
                {
                    string src2 = (string.IsNullOrEmpty(item.NavigateUrl) ? "#" : item.NavigateUrl.Replace("~", "."));
                    litMenu.Text += @"<a class=""dropdown-item"" " +                             
                             @""" href=""" + src2 + @"""></a>";
                }

                litMenu.Text += "</div>";
            }
            else
            {
                litMenu.Text += @"<li class=""nav-item active""><a class=""nav - link"" " +
                            @""" rel=""" + (men.Parent != null ? men.Parent.Value.Replace(" ", "_") : "") +
                            @""" href=""" + src + @"""></a>";
            }

            litMenu.Text += @"</li>";
        }
    }
}