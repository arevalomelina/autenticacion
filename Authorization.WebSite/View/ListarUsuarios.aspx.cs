﻿using Authorization.WebSite.AuthorizationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Authorization.WebSite.View
{
    public partial class ListarUsuarios : System.Web.UI.Page
    {



        Usuario[] usuarios
        {
            get
            {
                return (Usuario[])ViewState["usuarios"];
            }
            set
            {
                ViewState["usuarios"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                usuarios = fnListarUsuarios(null, null, null);
                gvUsers.DataSource = usuarios;
                gvUsers.DataBind();

                ListView1.DataSource = usuarios;
                ListView1.DataBind();
                List<string> lstNombres = (from p in usuarios
                                           select p.UserName).ToList();
                ddlUserName.DataSource = lstNombres;
                ddlUserName.DataBind();

            }
        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.DataSource = usuarios;
            gvUsers.PageIndex = e.NewPageIndex;
            gvUsers.DataBind();
        }
        protected void ListView1_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            //set current page startindex, max rows and rebind to false
            lvDataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

            //rebind List View
            ListView1.DataSource = usuarios;
            ListView1.DataBind();
        }

        protected void ddlUserName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Usuario[] usuarios = fnListarUsuarios(null, ddlUserName.Text, null);
            gvUsers.DataSource = usuarios;
            gvUsers.DataBind();
        }

        protected void chkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Usuario[] usuarios = fnListarUsuarios(null, null, chkStatus.Checked);
            gvUsers.DataSource = usuarios;
            gvUsers.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Usuario[] usuarios = fnListarUsuarios(int.Parse(txtId.Text), null, null);
            gvUsers.DataSource = usuarios;
            gvUsers.DataBind();
        }

        protected void btnBuscarTodos_Click(object sender, EventArgs e)
        {
            Usuario[] usuarios = fnListarUsuarios(int.Parse(txtId.Text==""?"0": txtId.Text), ddlUserName.Text, chkStatus.Checked);
            gvUsers.DataSource = usuarios;
            gvUsers.DataBind();
        }

        private Usuario[] fnListarUsuarios(int? id, string username, bool? estado)
        {

            using (var svc = new AuthorizationServiceClient())
            {
                Usuario[] usuarios = svc.ListarUsuariosFiltros(id, username, estado);
                return usuarios;
            }
        }
    }
}