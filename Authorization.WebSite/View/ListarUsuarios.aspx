﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Authorization.Master" AutoEventWireup="true" CodeBehind="ListarUsuarios.aspx.cs" Inherits="Authorization.WebSite.View.ListarUsuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .pagination-ys {
            /*display: inline-block;*/
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

            .pagination-ys table > tbody > tr > td {
                display: inline;
                border: none;
            }

                .pagination-ys table > tbody > tr > td > a,
                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    color: #dd4814;
                    background-color: #ffffff;
                    border: 1px solid #dddddd;
                    margin-left: -1px;
                }

                .pagination-ys table > tbody > tr > td > span {
                    position: relative;
                    float: left;
                    padding: 8px 12px;
                    line-height: 1.42857143;
                    text-decoration: none;
                    margin-left: -1px;
                    z-index: 2;
                    color: #aea79f;
                    background-color: #f5f5f5;
                    border-color: #dddddd;
                    cursor: default;
                }

                .pagination-ys table > tbody > tr > td:first-child > a,
                .pagination-ys table > tbody > tr > td:first-child > span {
                    margin-left: 0;
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td:last-child > a,
                .pagination-ys table > tbody > tr > td:last-child > span {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                }

                .pagination-ys table > tbody > tr > td > a:hover,
                .pagination-ys table > tbody > tr > td > span:hover,
                .pagination-ys table > tbody > tr > td > a:focus,
                .pagination-ys table > tbody > tr > td > span:focus {
                    color: #97310e;
                    background-color: #eeeeee;
                    border-color: #dddddd;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Grid view</h1>
    <div>
        <h4>Filtros</h4>
        <asp:TextBox ID="txtId" runat="server" TextMode="Number"></asp:TextBox>
        <%--<asp:Button ID="btnSearch" runat="server" Text="Buscar" OnClick="btnSearch_Click" />--%>
        <asp:DropDownList ID="ddlUserName" runat="server" AutoPostBack="False" OnSelectedIndexChanged="ddlUserName_SelectedIndexChanged"></asp:DropDownList>
        <asp:CheckBox ID="chkStatus" runat="server" AutoPostBack="False" OnCheckedChanged="chkStatus_CheckedChanged" Text="Activo" />
        <asp:Button ID="btnBuscarTodos" runat="server" Text="All" OnClick="btnBuscarTodos_Click" />
    </div>
    <asp:GridView ID="gvUsers" runat="server" CssClass="table table-bordered bs-table" AllowPaging="True" OnPageIndexChanging="gvUsers_PageIndexChanging" PageSize="2" AutoGenerateColumns="false">
        <PagerSettings PageButtonCount="2" />
        <PagerStyle CssClass="pagination-ys" />
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="UsuarioId" InsertVisible="False" ReadOnly="True" SortExpression="Id" ControlStyle-Width="600px" />
            <asp:BoundField DataField="UserName" HeaderText="Username" SortExpression="Username" ControlStyle-Width="600px" />
            <asp:CheckBoxField DataField="Estado" HeaderText="Estado" SortExpression="Estado" ControlStyle-Width="600px" />
        </Columns>
    </asp:GridView>
    <h1>List View</h1>
    <asp:ListView ID="ListView1" runat="server" OnPagePropertiesChanging="ListView1_PagePropertiesChanging">
        <ItemTemplate>
            <div style="float: left; width: 300px;">
                <asp:Label ID="FirstNameLabel" runat="server"
                    Text='<%#Eval("Id") %>' />
                <br />
                <asp:Label ID="Label1" runat="server"
                    Text='<%#Eval("UserName") %>' />
            </div>
        </ItemTemplate>
    </asp:ListView>
    <asp:DataPager ID="lvDataPager1" runat="server" PagedControlID="ListView1" PageSize="6" class="pagination-ys">
        <Fields>
            <asp:NumericPagerField ButtonType="Button" />
        </Fields>
    </asp:DataPager>
</asp:Content>
