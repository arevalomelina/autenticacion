﻿using Authentication.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Authorization.WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IAuthorizationService" en el código y en el archivo de configuración a la vez.
    [ServiceContract(Namespace ="http://viamericas.com")]
    public interface IAuthorizationService
    {
        [OperationContract]
        bool CrearUsuario(Usuario objUsuario, int RolId);
        [OperationContract]
        List<Usuario> ListarUsuarios();
        [OperationContract]
        bool ValidarNombreUsuario(string username);
        [OperationContract]
        Usuario ObtenerUsuario(string username);
        [OperationContract]
        string ObtenerHash(string cadena);
        [OperationContract]
        List<Menu> ObtenerMenu(string username);
        [OperationContract]
        List<Usuario> ListarUsuariosFiltros(int? id, string userName, bool? estado);
        [OperationContract]
        Usuario ObtenerUsuarioId(int id);
    }
}
