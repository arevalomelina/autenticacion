﻿using Authentication.Model;
using Authentication.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Authorization.WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "AuthorizationService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione AuthorizationService.svc o AuthorizationService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class AuthorizationService : IAuthorizationService
    {
        public bool CrearUsuario(Usuario objUsuario, int RolId)
        {
            return new AuthenticationModel().CrearUsuario(objUsuario, RolId);
        }

        public List<Usuario> ListarUsuarios()
        {
            return new AuthenticationModel().ListarUsuarios();
        }

        public bool ValidarNombreUsuario(string username)
        {
            return new AuthenticationModel().ValidarNombreUsuario(username);
        }

        public Usuario ObtenerUsuario(string username)
        {
            return new AuthenticationModel().ObtenerUsuario(username);
        }

        public string ObtenerHash(string cadena)
        {
            return new AuthenticationModel().ObtenerHash(cadena);
        }

        public List<Menu> ObtenerMenu(string username)
        {
            return new AuthenticationModel().ObtenerMenu(username);
        }

        public List<Usuario> ListarUsuariosFiltros(int? id, string userName, bool? estado)
        {
            return new AuthenticationModel().ListarUsuariosFiltros(id, userName, estado);
        }

        public Usuario ObtenerUsuarioId(int id)
        {
            return new AuthenticationModel().obtenerUsuarioId(id);
        }
    }
}
