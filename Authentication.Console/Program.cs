﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var svc = new AuthorizationSvc.AuthorizationServiceClient())
            {
                string hashq = svc.ObtenerHash("Melina12345");
            }
            string password = "Shx5NKHHjenAV5A";
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            string hash =  Convert.ToBase64String(dst);
        }
    }
}
