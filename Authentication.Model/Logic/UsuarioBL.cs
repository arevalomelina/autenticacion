﻿using Authentication.Model.Data;
using Authentication.Model.Entities;
using Authentication.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Model.Logic
{
    internal class UsuarioBL
    {
        LogHelper log = new LogHelper();

        internal bool CrearUsuario(Usuario objUsuario, int RolId)
        {
            try
            {
                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("psUsername", objUsuario.UserName);
                parametros.Add("psPassword", new SecurityHelper().getHash(objUsuario.Password));
                parametros.Add("piRolId", RolId);
                parametros.Add("pbFotoPerfil", objUsuario.FotoPerfil);
                new Dao().EjecutarStoredProcedure(Constantes.spCrearUsuario, parametros);
                return true;
            }
            catch (Exception ex)
            {
                log.EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                return false;
            }
        }

        internal List<Usuario> ListarUsuarios()
        {
            try
            {
                string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado]FROM[security].[Usuarios]";

                DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

                List<Usuario> lstUsuarios = new List<Usuario>();
                Usuario usuario;
                foreach (DataRow fila in dtUsuarios.Rows)
                {
                    usuario = new Usuario();
                    usuario.Id = fila["UsuarioId"].ToString();
                    usuario.UserName = fila[1].ToString();
                    usuario.Password = fila["Password"].ToString();
                    usuario.Estado = bool.Parse(fila["Estado"].ToString());
                    usuario.FotoPerfil = fila["FotoPerfil"]==null?null:(byte[])fila["FotoPerfil"];
                    lstUsuarios.Add(usuario);
                }

                return lstUsuarios;
            }
            catch (Exception ex)
            {
                log.EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                return null;
            }
        }

        internal List<Usuario> ListarUsuariosFiltros(int? id, string userName, bool? estado)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("piUserId", id);
            parametros.Add("psUserName", userName);
            parametros.Add("pbStatus", estado);
            DataTable dtUsuario = new Dao().EjecutarStoredProcedureDT(Constantes.spListarUsuariosFiltro, parametros);

            List<Usuario> lstUsuarios = new List<Usuario>();
            Usuario usuario;
            foreach (DataRow fila in dtUsuario.Rows)
            {
                usuario = new Usuario();
                usuario.Id = fila["UsuarioId"].ToString();
                usuario.UserName = fila[1].ToString();
                usuario.Password = fila["Password"].ToString();
                usuario.Estado = bool.Parse(fila["Estado"].ToString());


                lstUsuarios.Add(usuario);
            }

            return lstUsuarios;

        }
        internal bool ValidarNombreUsuario(string username)
        {
            try
            {
                string sqlString = @"SELECT [UsuarioId] FROM[security].[Usuarios] WHERE [Username]='" + username + "'";

                DataTable dtUsuarios = new Dao().GetDataTable(sqlString);

                if (dtUsuarios.Rows.Count > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                log.EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                return false;
            }
        }

        internal Usuario obtenerUsuario(string username)
        {
            try
            {
                string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado], [FotoPerfil] FROM[security].[Usuarios] WHERE username = '" + username + "'";

                DataTable dtUsuarios = new Dao().GetDataTable(sqlString);
                Usuario usuario;
                usuario = new Usuario();
                Byte[] array = new Byte[64];
                Array.Clear(array, 0, array.Length);

                if (dtUsuarios.Rows.Count > 0)
                {
                    DataRow fila = dtUsuarios.Rows[0];

                    usuario.Id = fila["UsuarioId"].ToString();
                    usuario.UserName = fila[1].ToString();
                    usuario.Password = fila["Password"].ToString();
                    usuario.Estado = bool.Parse(fila["Estado"].ToString());
                    usuario.FotoPerfil = fila["FotoPerfil"] == DBNull.Value ? array : (byte[])fila["FotoPerfil"];

                }
                return usuario;
            }
            catch (Exception ex)
            {
                log.EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                return null;
            }
        }

        internal Usuario obtenerUsuarioId(int id)
        {
            try
            {
                string sqlString = @"SELECT [UsuarioId],[Username],[Password],[Estado], [FotoPerfil] FROM[security].[Usuarios] WHERE UsuarioId = " + id.ToString() + "";

                DataTable dtUsuarios = new Dao().GetDataTable(sqlString);
                Usuario usuario;
                usuario = new Usuario();
                if (dtUsuarios.Rows.Count > 0)
                {
                    DataRow fila = dtUsuarios.Rows[0];

                    usuario.Id = fila["UsuarioId"].ToString();
                    usuario.UserName = fila[1].ToString();
                    usuario.Password = fila["Password"].ToString();
                    usuario.Estado = bool.Parse(fila["Estado"].ToString());
                    usuario.FotoPerfil = fila["FotoPerfil"] == null ? null : (byte[])fila["FotoPerfil"];

                }
                return usuario;
            }
            catch (Exception ex)
            {
                log.EscribirLog(LogType.error, (ex.InnerException ?? ex).Message);
                return null;
            }
        }

    }
}

