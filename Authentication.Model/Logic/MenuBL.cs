﻿using Authentication.Model.Data;
using Authentication.Model.Entities;
using Authentication.Model.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Model.Logic
{
    internal class MenuBL
    {
        internal List<Menu> ObtenerMenu(string username)
        {
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("ps_username", username);
            DataTable dtMenu = new Dao().EjecutarStoredProcedureDT(Constantes.spObtenerMenu, parametros);

            List<Menu> menu = new List<Menu>();
            Menu item;
            foreach (DataRow fila in dtMenu.Rows)
            {
                item = new Menu();
                item.AsegurableId = int.Parse(fila["AsegurableId"].ToString());
                item.Nombre = fila["Nombre"].ToString();
                item.ParentId = int.Parse(string.IsNullOrWhiteSpace(fila["ParentId"].ToString()) ? "0" : fila["ParentId"].ToString());
                item.Ruta = fila["Ruta"].ToString();

                menu.Add(item);
            }

            return menu;
        }

    }
}
