﻿
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Authentication.Model.Entities
{
    [DataContract(Namespace = "http://viamericas.com", Name = "Usuario")]
    public class Usuario : IUser
    {
        [DataMember(IsRequired =true,Order =1)]
        [Required]
        [MaxLength(10)]
        public string Id { get; set; }
        [DataMember(IsRequired = true, Order = 2)]
        [Required]
        [MaxLength(10)]
        public string UserName { get; set; }
        [DataMember(IsRequired = true, Order = 3)]
        [Required]
        [MaxLength(10)]
        public string Password { get; set; }
        [DataMember(IsRequired = true, Order = 4)]
        [Required]
        [MaxLength(10)]
        public bool Estado { get; set; }
        [DataMember(IsRequired = false, Order = 5)]
        public byte[] FotoPerfil { get; set; }
    }
}
