﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Authentication.Model.Utilities
{
   internal class SerializerHelper
    {
        /// <summary>
        /// Permite serializar un objeto
        /// </summary>
        /// <param name="objeto">System.Object objeto</param>
        /// <param name="t">System.Type t</param>
        /// <returns>Retorna SerializeObject</returns>
        public static string Serialize<T>(T value)
        {

            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Permite deserealizar un objeto
        /// </summary>
        /// <typeparam name="T">System.Type t tipo del </typeparam>
        /// <param name="xml">xml para deserealizar</param>
        /// <returns> Retorna el objeto correspondiente</returns>
        public static T Deserialize<T>(string xml)
        {

            if (string.IsNullOrEmpty(xml))
            {
                return default(T);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlReaderSettings settings = new XmlReaderSettings();
            // No settings need modifying here

            using (StringReader textReader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        /// <summary>
        /// Permite convertir una colección a un objeto determiado
        /// </summary>
        /// <typeparam name="T">System.Type tipo de objeto a retornar</typeparam>
        /// <param name="collection">colección a serializar</param>
        /// <returns>Objeto solicitado</returns>
        public static T GetObject<T>(NameValueCollection collection)
        {
            Type type = typeof(T);
            var obj = Activator.CreateInstance(type);

            for (int i = 0; i < collection.Count; i++)
            {
                if (collection.GetKey(i) != null)
                {
                    var item = type.GetProperty(collection.GetKey(i));
                    if (item != null)
                        type.GetProperty(collection.GetKey(i)).SetValue(obj, collection.Get(i));
                }
            }
            return (T)obj;
        }
    }
}
