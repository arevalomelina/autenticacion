﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Model.Utilities
{
   internal static class Constantes
    {
        internal const string spLogin = "security.spAutenticarUsuario";
        internal const string spCrearUsuario = "security.CrearUsuario";
        internal const string spObtenerMenu = "security.ObtenerMenu";
        internal const string spInsertXml = "dbo.InsertarXmlTipo ";
        internal const string spListarProductos = "dbo.spListarProductos";
        internal const string spListarUsuariosFiltro = "security.ListarUsuariosFiltro";
    }
}
