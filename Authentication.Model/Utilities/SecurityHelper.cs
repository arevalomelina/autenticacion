﻿using System.Security.Cryptography;
using System.Text;

namespace Authentication.Model.Utilities
{
    internal class SecurityHelper
    {
        internal string getHash(string cadena)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(cadena));
                var sb = new StringBuilder();

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }
    }
}
