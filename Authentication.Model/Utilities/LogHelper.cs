﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace Authentication.Model.Utilities
{
    public class LogHelper
    {
        private string path = ConfigurationManager.AppSettings["RutaLog"].ToString();

        public void EscribirLog(LogType type, string mensaje)
        {
            string nombre = "Log-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".txt";
            string logMessage = DateTime.Now.ToString() + " ";
            logMessage = logMessage + type.ToString() + " ";
            logMessage = logMessage + mensaje;
            try
            {
                using (StreamWriter sw = new StreamWriter(path + nombre, true))
                {
                    sw.WriteLine(logMessage);
                    sw.Close();
                }
            }
            catch { }
        }

        public string LeerLog()
        {
            string nombre = "Log-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + ".txt";
            StringBuilder builder = new StringBuilder();
            string s;
            using (StreamReader sr = new StreamReader(path + nombre))
            {
                s = "";
                while ((s = sr.ReadLine()) != null)
                    builder.AppendLine(s);
            }

            return builder.ToString();
        }


    }
    public enum LogType
    {
        info = 1,
        advertencia = 2,
        error = 3,
        fatal = 4
    }

}
