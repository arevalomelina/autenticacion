﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Model.Data
{
    internal class Dao
    {
        readonly string Cadena;

        SqlConnection conexion;
        SqlCommand comando;
        SqlDataAdapter adapter;
        DataTable dataTable;


        internal Dao()
        {
            Cadena = ConfigurationManager.ConnectionStrings["CedesistemasDB"].ConnectionString;
        }

        /// <summary>
        /// Permite obtener el resultado de una consulta
        /// </summary>
        /// <param name="strSql">Query a ejecutar en la bd</param>
        /// <returns>Resultado de la Consulta</returns>
        public DataTable GetDataTable(string strSql)
        {
            dataTable = new DataTable();

            try
            {
                using (conexion = new SqlConnection(Cadena))
                {
                    //cnn.Open();
                    using (comando = conexion.CreateCommand())
                    {
                        comando.CommandText = strSql;
                        comando.CommandType = CommandType.Text;
                        using (adapter = new SqlDataAdapter(comando))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void EjecutarStoredProcedure(string nombreSp, Dictionary<string, object> parametros)
        {
            try
            {
                using (conexion = new SqlConnection(Cadena))
                {
                    //cnn.Open();
                    using (comando = conexion.CreateCommand())
                    {
                        conexion.Open();
                        comando.CommandText = nombreSp;
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.CommandTimeout = 9000000;
                        if (parametros != null)
                        {
                            //parametros para el sp
                            foreach (var item in parametros)
                            {
                                //se agrega el parametro que se tiene en el key en la posicion del parametro
                                comando.Parameters.AddWithValue(item.Key, item.Value);
                            }
                        }
                        comando.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
        }

        internal DataTable EjecutarStoredProcedureDT(string nombreSp, Dictionary<string, object> parametros)
        {
            dataTable = new DataTable();

            try
            {
                using (conexion = new SqlConnection(Cadena))
                {
                    //cnn.Open();
                    using (comando = conexion.CreateCommand())
                    {
                        conexion.Open();
                        comando.CommandText = nombreSp;
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.CommandTimeout = 9000000;
                        if (parametros != null)
                        {
                            //parametros para el sp
                            foreach (var item in parametros)
                            {
                                //se agrega el parametro que se tiene en el key en la posicion del parametro
                                comando.Parameters.AddWithValue(item.Key, item.Value);
                            }
                        }

                        using (adapter = new SqlDataAdapter(comando))
                        {
                            adapter.Fill(dataTable);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

            return dataTable;
        }

        internal bool UpdateDataTable(DataTable poDataSet, String psQueryTable)
        {

            SqlDataAdapter oAdapter;
            SqlCommand cmd;
            SqlCommandBuilder oCommandBuilder;
            SqlConnection _cnn;

            try
            {
                using (_cnn = new SqlConnection(Cadena))
                {
                    _cnn.Open();

                    cmd = new SqlCommand(psQueryTable, _cnn);
                    oAdapter = new SqlDataAdapter(cmd);
                    oCommandBuilder = new SqlCommandBuilder(oAdapter);
                    oAdapter.Update(poDataSet);
                }

                return (true);

            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
