﻿using Authentication.Model.Entities;
using Authentication.Model.Logic;
using Authentication.Model.Utilities;
using System.Collections.Generic;

namespace Authentication.Model
{
    public class AuthenticationModel
    {

        public bool CrearUsuario(Usuario objUsuario, int RolId)
        {
            return new UsuarioBL().CrearUsuario(objUsuario, RolId);
        }

        public List<Usuario> ListarUsuarios()
        {
            return new UsuarioBL().ListarUsuarios();
        }

        public bool ValidarNombreUsuario(string username)
        {
            return new UsuarioBL().ValidarNombreUsuario(username);
        }

        public Usuario ObtenerUsuario(string username)
        {
            return new UsuarioBL().obtenerUsuario(username);
        }

        public string ObtenerHash(string cadena)
        {
            return new SecurityHelper().getHash(cadena);
        }

        public List<Menu> ObtenerMenu(string username)
        {
            return new MenuBL().ObtenerMenu(username);
        }

        public List<Usuario> ListarUsuariosFiltros(int? id, string userName, bool? estado)
        {
            return new UsuarioBL().ListarUsuariosFiltros(id, userName, estado);
        }
        public Usuario obtenerUsuarioId(int id)
        {
            return new UsuarioBL().obtenerUsuarioId(id);
        }
    }
}
